using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    public GameObject[] Panels;
    public bool isIntro;

    public GameObject objectoEscena;

    public void Start()
    {
        isIntro = true;
    }
    public void Update()
    {
        if (isIntro == true && Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine("IntroLoadingScreen");
        }   
        else if(isIntro == false)
        {
            
        }
    }

    public void MenuToSceneSelect()
    {
        Panels[1].SetActive(false);
        Panels[2].SetActive(true);
    }

    public void SceneSelectToMenu()
    {
        Panels[2].SetActive(false);
        Panels[1].SetActive(true);
    }

    public void ToSphereScene()
    {
        SceneManager.LoadScene("EscenaEsfera");
    }

    public void ToCubeScene()
    {
        SceneManager.LoadScene("EscenaCubo");
    }

    public void ExitGame()
    {       
        Application.Quit();
        Debug.Log("Application Quit.");
    }

    public IEnumerator IntroLoadingScreen()
    {
        Panels[0].SetActive(false);
        Panels[3].SetActive(true);
        isIntro = false;
        yield return new WaitForSeconds(1);
        Panels[3].SetActive(false);
        Panels[1].SetActive(true);
    }

    public void PauseMenu()
    {
        Panels[0].SetActive(false);
        objectoEscena.SetActive(false);
        Panels[1].SetActive(true);
    }

    public void ReturnFromPauseMenu()
    {
        Panels[1].SetActive(false);
        objectoEscena.SetActive(true);
        Panels[0].SetActive(true);
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
